# Ding
Recurring desktop notifications

## Design sketch
- Reads a directory of conf files that specify reminder properties:
  - Name
  - Notification
    - Summary (<= 40 char)
    - Body (multiline, optional)
    - Sound
      - "github.com/hajimehoshi/oto"
    - Icon
  - Schedule
    - Start (first instance)
    - Interval
    - End (last instance, can be nil for infinite)
    - Use cron as inspiration?
- CLI
  - Add new notifications and write conf
  - List notifications loaded
  - Delete a notification and its conf
  - Disable a notification (have a 'disabled' subfolder of conf folder?)
  - Edit a notification (probably really, delete old conf and write a new one)
- systemd/sdnotify compatible
  - "github.com/coreos/go-systemd/daemon"
  - How to daemonize w/o systemd?

## References/Inspiration/Examples
[](https://godoc.org/github.com/godbus/dbus)
[](https://developer.gnome.org/notification-spec)
[](https://github.com/esiqveland/notify/blob/master/notification.go)
[](https://github.com/lukechampine/hey/blob/master/hey.go)
[](https://github.com/TheCreeper/go-notify/blob/master/notify.go)
