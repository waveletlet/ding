package main

import (
	"fmt"
	yaml "gopkg.in/yaml.v2"
	"io/ioutil"
	"time"
)

func (rem *Reminder) ReadConfig(path string) (*Reminder, error) {
	configFile, err := ioutil.ReadFile(path)
	if err != nil {
		return rem, err
	}

	//err = yaml.Unmarshal(configFile, &rem)
	err = yaml.Unmarshal(configFile, &rem)
	if err != nil {
		return rem, err
	}

	return rem, nil
}

func (rem *Reminder) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var incoming struct {
		Name         string
		Notification struct {
			Summary string `yaml:"text-summary"` // single line, preferably <= 40 chars
			Timeout string // ms to display notification. 0 is persistent, -1 is system default
			Body    string `yaml:"text-body"` // multi line, paragraphs separated by single line breaks. can contain xml based markup (see spec)
			AppName string
			Icon    string // https://developer.gnome.org/notification-spec/#icons-and-images-format
			//Actions []string               // placeholder. Is this required?
			//Hints   map[string]interface{} //placeholder.
		}
		Schedule struct {
			Start    string
			Interval string
			End      string
		}
	}
	if err := unmarshal(&incoming); err != nil {
		return err
	}

	// process the values in the incoming struct and put them in the Reminder
	// also, error checking

	// Assuming RFC3339 because that's how go-yaml marshals into text, would be
	// good to be more flexible in the future
	var start time.Time
	start, err := time.Parse(time.RFC3339, incoming.Schedule.Start)
	if err != nil {
		if len(incoming.Schedule.Start) == 0 {
			start = time.Time{}
		} else {
			return err
		}
	}

	var end time.Time
	end, err = time.Parse(time.RFC3339, incoming.Schedule.End)
	if err != nil {
		if len(incoming.Schedule.End) == 0 {
			end = time.Time{}
		} else {
			return err
		}
	}

	interval, err := time.ParseDuration(incoming.Schedule.Interval)
	if err != nil {
		return err
	}

	rem.Name = incoming.Name
	rem.Notification.Summary = incoming.Notification.Summary
	rem.Notification.Body = incoming.Notification.Body
	rem.Schedule.Start = start
	rem.Schedule.End = end
	rem.Schedule.Interval = interval

	fmt.Printf("Unmarshalled yaml to: %v\n", rem)

	return nil
}
