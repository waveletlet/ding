package main

import (
	"fmt"
	"path/filepath"
	"time"

	"github.com/godbus/dbus"
)

type Reminder struct {
	Name         string
	Notification Notification
	Ticker       *time.Ticker
	Timers       []*time.Timer
	Schedule     Schedule
}

type Schedule struct {
	Start    time.Time
	Interval time.Duration
	End      time.Time
}

func (rem *Reminder) Enqueue() {
	//TODO check end time first to make sure it's not in the past before setting
	//up the rest of this
	startIn := rem.Schedule.Start.Sub(time.Now())
	startTimer := time.AfterFunc(startIn, rem.RunSchedule)
	rem.Timers = append(rem.Timers, startTimer)

	//TODO this probably also needs some checking to make sure the times are sane,
	//like the start time is before the end time so the ticker definitely exists
	//before the endtimer stops it. Maybe that should be in the config validation.
	if !rem.Schedule.End.IsZero() {
		endIn := rem.Schedule.End.Sub(time.Now())
		// Get a nil pointer deref if I use a timer to stop the ticker here, not
		// sure why. Saw a similar issue with a timer and a ticker fighting over a
		// lock in the Go source, but it's supposed to have been fixed.
		//
		// Alternative: drain the endTimer in func main() where I'm currently using a
		// for{} to keep the program open forever. Something like:
		//   <- endTimer.C
		//   rem.Ticker.Stop()
		//  or a select case watching the channels of all the tickers.
		//
		//	endTimer := time.AfterFunc(endIn, func() {
		go func() {
			time.Sleep(endIn) // because we can't use a timer
			fmt.Printf("%v endTimer at %v\n", rem.Name, time.Now())
			fmt.Println(rem.Name)
			fmt.Println(rem.Ticker)
			rem.Ticker.Stop()
		}()
		//rem.Timers = append(rem.Timers, endTimer)
	}
}

func (rem *Reminder) RunSchedule() {
	tkr := time.NewTicker(rem.Schedule.Interval)
	rem.Ticker = tkr
	go func() {
		rem.Notification.Push()
		for _ = range tkr.C {
			rem.Notification.Push()
			fmt.Printf("%v tick at %v\n", rem.Name, time.Now())
		}
	}()
}

type Notification struct {
	// See https://developer.gnome.org/notification-spec/
	Summary string `yaml:"text-summary"` // single line, preferably <= 40 chars
	Timeout int32  // ms to display notification. 0 is persistent, -1 is system default

	//optional
	Body    string `yaml:"text-body"` // multi line, paragraphs separated by single line breaks. can contain xml based markup (see spec)
	AppName string
	ID      uint32
	Icon    string // https://developer.gnome.org/notification-spec/#icons-and-images-format

	Actions []string               // placeholder. Is this required?
	Hints   map[string]interface{} //placeholder.
	//Potentially useful hints from the standard set:
	//sound-file string
	//sound-name string
	//supress-sound bool
	//urgency int

}

func (not Notification) Push() error {
	conn, err := dbus.SessionBus()
	if err != nil {
		return err
	}
	// In theory, system could be running a different notification server and this
	// shouldn't be hardcoded.
	call := conn.Object("org.freedesktop.Notifications", "/org/freedesktop/Notifications").Call(
		"org.freedesktop.Notifications.Notify", 0,
		not.AppName,
		not.ID,
		not.Icon,
		not.Summary,
		not.Body,
		not.Actions,
		not.Hints,
		not.Timeout,
	)

	if err := call.Err; err != nil {
		return err
	}

	if len(call.Body) == 1 {
		if u, ok := call.Body[0].(uint32); ok {
			not.ID = u
		}
	}
	return nil
}

func main() {
	var reminders []*Reminder

	// TODO accept flags specifying conf dir
	remConfs, _ := filepath.Glob("./*.yaml")
	for _, rc := range remConfs {
		rem := &Reminder{}
		rem, err := rem.ReadConfig(rc)
		if err != nil {
			fmt.Println(err)
		}
		reminders = append(reminders, rem)

		fmt.Printf("rem: %s\n", rem.Name)
		fmt.Printf("rem: %s\n", rem.Notification.Summary)

		rem.Enqueue()
	}

	// run forever...
	for {
	}
	// TODO handle signals (kill, reload...)
}
